<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Order extends Model
{
    protected $fillable = [
        'name', 'amount', 'status', 'phone', 'reference', 'items', 'date', 'address'
    ];

    public function users() {
        return $this->belongsTo(User::class);
    }
}
