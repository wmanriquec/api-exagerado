<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index()
    {
        $orders = Order::with('users')->get();
        return response()->json(['data' => $orders]);
    }

    public function show($id)
    {
        $orders = Order::where('id',$id)->first();
        return response()->json(['data' => $orders]);
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'phone' => 'required',
            'amount' => 'required',
            'items' => 'required',
            'address' => 'required',
        ]);
        
        if ($validator->fails()) {    
            return response()->json($validator->messages(), 400);
        }
        

        $order = Order::create($request->only([
            'name', 'phone', 'amount', 'items', 'address','reference'
        ]));

        $order->save();

        return response()->json(['message' => 'Completamos tu solicitud, nos comunicaremos en breve contigo']);

    }

    public function store(Request $request)
    {
        //
    }


    public function finish($id)
    {
        $order = Order::find($id);
        $order->status="terminado";
        $order->update();
        return response()->json(['data' => $order ,'message' => 'Order item successfully updated']);


    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
